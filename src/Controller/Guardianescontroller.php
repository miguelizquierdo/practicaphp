<?php
namespace App\Controller;

use App\Entity\Guardianes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('guardianes')]
class Guardianescontroller extends AbstractController{
    
    #[Route('/nueva', name:'newguardian')]
    public function newguardina(EntityManagerInterface $em){
        $guard= new Guardianes();
        $guard->setAutor("Pedro");
        $guard->setComentario("Experiencia fantástica");
        $guard->setCiudad("Madrid");
        $em->persist($guard);
        $em->flush($guard);

        return new Response("Guardian bien insertado");
    }

    #[Route('/listado', name:'listguardian')]
    public function listguardianes(EntityManagerInterface $em){        
        $repo=$em->getRepository(Guardianes::class);
        $guard=$repo->findAll();

        return $this->render("demo/demo.solicitudes.html.twig", ["guard" => $guard]);
    }
} 