<?php
namespace App\Controller;

use App\Entity\Demo;
use App\Form\DemoFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Democontroller extends AbstractController{ 

    #[Route('demo/listado', name:'listdemo')]
    public function listdemo(EntityManagerInterface $em){        
        $repo=$em->getRepository(Demo::class);
        $demos=$repo->findAll();

        return $this->render("demo/demo.solicitudes.html.twig", ["demo" => $demos]);
    }

    #[Route('/', name:'newdemo')]
    public function newdemo(Request $request, EntityManagerInterface $em){  
        $form= $this->createForm(DemoFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $demo=$form->getData();
            $em->persist($demo);
            $em->flush();
            $this->addflash("ok", "Demo insertada correctamente");
            return $this->redirectToRoute("newdemo");
        }

        return $this->renderForm("home/home.html.twig", ["formdata" => $form]);        
    }

    #[Route('/demo/editar/{id}', name:'editardemo')]
    public function editardemo(Request $request, EntityManagerInterface $em, $id){  
        $repo=$em->getRepository(Demo::class);
        $demo=$repo->find($id);

        $form= $this->createForm(DemoFormType::class, $demo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $demo=$form->getData();
            $em->persist($demo);
            $em->flush();
            $this->addflash("ok", "Demo insertada correctamente");
            return $this->redirectToRoute("newdemo");
        }
        return $this->renderForm("demo/demoeditar.html.twig", ["formdata" => $form]);        
    }

    #[Route('/demo/borrar/{id}', name:'borrardemo')]
    public function borrardemo( EntityManagerInterface $em, $id){  
        $repo=$em->getRepository(Demo::class);
        $demo=$repo->find($id);
        $em->remove($demo);
        $em->flush();      

        return $this->redirectToRoute("newdemo");          
    }
} 