<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use App\Entity\Demo;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Demofixture extends Fixture
{
    protected $client;
    public function __construct(HttpClientInterface $http){
        $this->client=$http;        

    }

    public function load(ObjectManager $manager): void
    {
        $faker=Factory::create();
        for ($i=1; $i < 101 ; $i++) { 
            $demo=new Demo();
            $response=$this->client->request('GET',"https://pokeapi.co/api/v2/pokemon/$i/");
            $contenido=$response->toArray();
            $demo->setNombre($contenido['name']);
            $demo->setEmail($faker->email());
            $demo->setCiudad($faker->Word());
            $manager->persist($demo);
        }

        $manager->flush();
    }
}
