<?php

namespace App\DataFixtures;

use App\Entity\Guardianes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class GuardianesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker=Factory::create();
        for ($i=0; $i < 100 ; $i++) { 
            $guard=new Guardianes();

            $guard->setComentario($faker->Text(100));
            $guard->setAutor($faker->Word());
            $guard->setCiudad($faker->Word());
            $manager->persist($guard);
        }

        
        $manager->flush();
    }
}

