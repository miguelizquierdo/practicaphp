<?php

namespace App\Form;

use App\Entity\Demo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DemoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre', TextType::class, [
                    'attr' => ['placeholder' => 'Vicent Chase'],])
            ->add('email', EmailType::class, [
                'attr' => ['placeholder' => 'vicent.chase@gmail.com'],])
            ->add('ciudad', TextType::class, [
                'attr' => ['placeholder' => 'Madrid'],])
            ->add('LOPD', CheckboxType::class, [ 
                   'label' => 'Aceptas la política de privacidad', 'mapped' => false])           
            ->add('Enviar', SubmitType::class, [
                'attr' => ['class' => 'save'],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Demo::class,
        ]);
    }
}
